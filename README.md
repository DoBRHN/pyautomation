# PyAutomation

<p align="center"><img width=500 alt="PyAutomation" src="https://gitlab.com/DoBRHN/pyautomation/-/raw/main/Design/GUI.PNG"></img></p>

PyAutomation est un script permettant de déployer rapidement des lignes de configurations sur un système cible.

L'utilisation de solution DevOps type **Ansible** sont des outils puissants mais nécessite une installation plus ou moins importante avec d'être exploitée. 

PyAutomation répond à un besoin rapide d'automatisation, pour déployer des lignes de commandes sur des switchs distants.

## Requirements

Le code est développé en version **3.10.1** de Python. 
Ci-dessous les modules de base ainsi que les librairies nécessaire à l'utilisation du script

| Modules / Librairies    | Version                                                                                        | 
| :---------------------- | :--------------------------------------------------------------------------------------------- | 
| **os**                  |                                                                                                | 
| **Pathlib**             |                                                                                                |
| **datetime**            |                                                                                                |
| **PySimpleGUI**         | 4.60.1                                                                                         |
| **netmiko**             | 4.1.1                                                                                          | 

```
pip install -r requirements.txt

```

## Informations

**IMPORTANT** : Le module netmiko est utilisé pour se connecter et injecter des configurations. Lors de l'exploitation du script, vous ête directement en mode de configuration global **switch(config)**.

Il s'agit d'une première version, seul les systèmes **Cisco IOS, NXOS et AOS (6 et 8)** ont été testés. Bien entendu, le module Netmiko fonctionne avec de nombreux autres systèmes. Voir la rubrique **upgrade** pour ajouter de nouveaux systèmes.

## Usage

L'exploitation du programme nécessite uniquement une connectivité **SSH** entre votre poste et les switchs distants. 
Pour lancer le programme :
```
python main.py

```

Renseignez les champs, en respectant les syntaxes ci-dessous :

| Champs                  | Notes                                                                                          | 
| :---------------------- | :--------------------------------------------------------------------------------------------- | 
| **Selection OS**        | Il s'agit de **radio button**. Seul un type de système cible peut-être sélectionné                |   
| **Hôtes**               | Renseignez les IPs des hôtes distants en les séparants d'une virgule. Exemple : **192.168.1.1,192.168.1.2**                                                                                                                        |
| **Commandes**           | Identique aux hôtes, renseignez les commandes à injecter en les séparants par une virgule. Exemple : **show run,vlan 10**                                                                                                                       |
| **Login**               | Identifiants pour se connecter aux équipements                                                                                                                |
| **Password**            | Mot de passe pour se connecter aux équipements. **_Le champ n'est pas visible_**                                                                                                                | 
| **Output**              | Résultat obtenu des commandes déployées                                                                                                                  | 

**Context et exemple** : Je souhaite déployer le **vlan 10** sur **3** switchs Cisco avec le système **IOS** :
<p align="center"><img width=500 alt="Context" src="https://gitlab.com/DoBRHN/pyautomation/-/raw/main/Design/Context.PNG"></img></p>

Une fois tous les champs renseignés, vous pouvez appuyer sur "**DEPLOIEMENT**". Le résultat de la commande s'affichera dans la fenêtre "**Output**'. 
De plus, un répertoire "**DATA**" sera créé (**s'il n'éxiste pas déjà**) à la racine du script afin de stocker le résultat des commande de chacun de vos switch, sous la forme "**ip_date.txt**". _Cela à pour but d'avoir une trace après chaque modifications._

**IMPORTANT**: Lors du déploiement, le programme peut être dans l'état "**_ne pas repondre_**". Ne pas s'en préocupper et patientez le temps d'avoir un résultat dans le champ "**Output**". Les fichiers d'ouput se créent et s'incrémentent automatiquement à chaque injections. Vous pouvez consulter les fichiers avant la fin du programme, et ce, sans pertubation.

<p align="center"><img width=500 alt="Deploiement" src="https://gitlab.com/DoBRHN/pyautomation/-/raw/main/Design/File.PNG"></img></p>

**En cas d'échec de connexion, un message d'erreur vous sera retourné**. Vérifiez votre commande, login / password ainsi que la connectivité SSH.
<p align="center"><img width=500 alt="Echec" src="https://gitlab.com/DoBRHN/pyautomation/-/raw/main/Design/echec.PNG"></img></p>

## Upgrade

La librairie **Netmiko** est compatible avec de nombreux systèmes d'exloitations. Consultez la page suivante afin d'avoir plus d'informations :
https://github.com/ktbyers/netmiko/blob/develop/PLATFORMS.md

Le programme de base intègre uniquement les systèmes **Cisco IOS, NXOS et Alcatel AOS 6** et **8**. La force du code réside dans le fait que pour ajouter davantages de systèmes, il suffit uniquement d'ajouter un **radio button** dans le layout. **_Voir et suivre la ligne 17 (template) dans le fichier main.py_**

<p align="center"><img width=500 alt="Upgrade" src="https://gitlab.com/DoBRHN/pyautomation/-/raw/main/Design/upgrade.PNG"></img></p>

## Suivi de Versions

## 1.0

• Exploitation de la librairie **Netmiko**

• Compatible avec le protcole SSH uniquement

• Execution en mode de configuration global **switch(config)**

• Compatible **Cisco IOS, NXOS, AOS 6** et **8**

## Features

Ci-dessous les futures features à intégrer dans les prochaines versions :

• Ajout de la librairie **Paramiko** pour plus de flexibilité

• Compatibilité avec le protocole **Telnet**

• Compatibilité avec plus de systèmes d'exploitations (**Junos, Arista, Aruba OSS** et **OS-CX**)


## Contact et Disclaimer
Le script à été initialement développé pour sauvegarder les configurations d'un parc d'équipements réseaux. Je l'ai fait évoluer afin de répondre à un de mes besoins dans le cadre de mon travail.

Ce script n'offre aucune garantie. Je ne suis pas responsable de sa mauvaise utilisation ou de tout dommages éventuels causé par celui-ci. Exploitez le sur un environnement de maquette lors d'une première utilisation.

Pour plus d'informations, vous pouvez me trouver sur Linkedin via le lien suivant : https://fr.linkedin.com/in/dorian-bruchon

<p align="center"><img width=100 alt="Avatar" src="https://gitlab.com/DoBRHN/pyautomation/-/raw/main/Design/Avatar.png"></img></p>
